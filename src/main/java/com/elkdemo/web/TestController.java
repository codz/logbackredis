package com.elkdemo.web;

import com.elkdemo.service.LogDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by IBM on 2016/5/6.
 */
@Controller
public class TestController {

    @Autowired
    private LogDemoService logDemoService;

    @RequestMapping("/test")
    @ResponseBody
    public String test(){
        logDemoService.generateLog();
        logDemoService.jsonLog();
        return "hello";
    }
}
