package com.elkdemo;

import com.elkdemo.service.LogDemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.util.UUID;

/**
 * Created by IBM on 2016/5/6.
 */
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.elkdemo.service","com.elkdemo.web"})
public class Application{

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
